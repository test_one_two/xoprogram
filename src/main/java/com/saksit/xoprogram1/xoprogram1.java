/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saksit.xoprogram1;

import java.util.Scanner;

/**
 *
 * @author User
 */
        public class xoprogram1 {

            static boolean isFinish = false;
            static char winner = '-';
            static int row, col;
            static Scanner kb = new Scanner(System.in);
            static char[][] board = {{'-', '-', '-'},
            {'-', '-', '-'},
            {'-', '-', '-'}};

            static char player = 'X';

            static void showWelcome() {
                System.out.println("Welcome to OX Game");
            }

            static void showTable() {
                System.out.println("  1 2 3");
                for (int i = 0; i < board.length; i++) {
                    System.out.print(i + 1 + " ");
                    for (int j = 0; j < board.length; j++) {
                        System.out.print(board[i][j] + " ");
                    }
                    System.out.println("");
                }

            }

            static void showTurn() {
                System.out.println(player + " trun");
            }

            static void input() {

                while (true) {

                    System.out.println("Please input Row Col: ");
                    int row = kb.nextInt() -1;
                    int col = kb.nextInt() -1;
                    //System.out.println("row: "+ row +"col: "+col);
                    if (board[row][col] == '-') {
                        board[row][col] = player;
                        break;

                    }
                    System.out.println("Error");
                }
            }

            static void checkCol() {
                for (int row = 0; row < 3; row++) {
                    if (board[row][col] != player) {
                        return;
                    }
                }
                isFinish = true;
                winner = player;
            }

            static void checkRow() {
                for (int col = 0; col < 3; col++) {
                    if (board[row][col] != player) {
                        return;
                    }
                }
                isFinish = true;
                winner = player;
            }


            static void checkx() {

            }

            static void checkdraw() {

            }

            static void checkwin() {
                checkRow();
                checkCol();
                checkx();
            }

            static void switchPlayer() {
                if (player == 'X') {
                    player = 'O';
                } else {
                    player = 'X';
                }
            }

            static void showResult() {
                if (winner == '-') {
                    System.out.println("Draw!!!!!");
                } else {
                    System.out.println(winner + " winner");
                }
            }

            static void showBye() {
                System.out.println("Bye bye .....");
            }

            public static void main(String[] args) {
                showWelcome();
                do {
                    showTable();
                    showTurn();
                    input();
                    checkwin();
                    switchPlayer();
                } while (!isFinish);
                showResult();
                showBye();

            }
        }
